#[macro_use]
extern crate tileme;

use flexi_logger::{Logger, LogTarget, Criterion, Naming, Cleanup, Duplicate, opt_format};
use std::collections::HashMap;
use std::env;
use std::path::PathBuf;
use tileme::{Config, KeyCode, run_tray, WindowManager};


#[allow(unused_imports)]
fn main() {
    let appdata_folder = env::var("APPDATA").expect("APPDATA is not defined");
    let log_dir = PathBuf::from(format!(r"{}\tileme\logs", appdata_folder));
    match std::fs::metadata(&log_dir) {
        Ok(md) => if !md.is_dir() { panic!("Log directory path exists and not directory")},
        Err(_) => std::fs::create_dir_all(&log_dir).expect("Could not create log directory"),
    }
    Logger::with_env_or_str("info, tileme = debug")
        .log_target(LogTarget::File)
        .duplicate_to_stderr(Duplicate::Debug)
        .directory(log_dir)
        .rotate(
            Criterion::Size(10_000_000),
            Naming::Numbers,
            Cleanup::KeepLogFiles(10),
        )
        .format(opt_format)
        .start()
        .unwrap_or_else(|err| panic!("Logger initialization failed: {:?}", err));
    let desktops = vec!("A: Mail", "S: IM", "D: WWW", "F: Media", "G: Term", "Z", "X", "C", "V");
    let mut keybindings = HashMap::new();
    {
        use tileme::hotkey::{keys::*, modifiers::*};
        keybindings.insert(
            KeyCode { mask: ALT | SHIFT, code: 'Q' as u32 },
            run_internal!(exit)
        );
        for (i, desktop) in desktops.iter().enumerate() {
            keybindings.insert(
                KeyCode { mask: ALT, code: desktop.chars().next().unwrap() as u32 },
                run_internal!(go_to_desktop_number, i)
            );
            keybindings.insert(
                KeyCode { mask: ALT | SHIFT, code: desktop.chars().next().unwrap() as u32 },
                run_internal!(move_window_to_desktop, i)
            );
        }
        keybindings.insert(
            KeyCode { mask: ALT, code: 'K' as u32 },
            run_internal!(focus_prev),
        );
        keybindings.insert(
            KeyCode { mask: ALT, code: 'J' as u32 },
            run_internal!(focus_next),
        );
    }
    let config = Config {
        virtual_desktops: desktops,
    };
    run_tray(config, keybindings)
}
